import { IndeterminateCheckBoxSharp } from "@material-ui/icons";
import React from "react";
import {Table} from "react-bootstrap";
const ListBootStrap=()=>{
    const user=[
        {name:'Ajax',email:'ajax@test.com',phNo:497},
        {name:'Amir',email:'amir@test.com',phNo:487},
        {name:'Abid',email:'abid@test.com',phNo:477},
        {name:'Asif',email:'asif@test.com',phNo:467}
    ]
    return(<div>
        
               <h1>List With Bootstrap</h1>
               <Table striped variant="dark">
                   <tbody>
               <tr>
                  <td>Name</td>
                  <td>Email</td>
                  <td>Phone No</td>
                  
              </tr>
          {
              user.map((data,index)=>
              <tr key={index}>
                  <td >{index}.{data.name}</td>
                  <td >{data.email}</td>
                  <td >{data.phNo}</td>
                  
              </tr>
              )
          } 
          </tbody>
          </Table>
    </div>)
}
export default ListBootStrap;