import React,{PureComponent} from 'react';
class Pure extends PureComponent{
    constructor(){
        super();
        this.state={
            count:1
        }
    }
    render()
    {
        console.warn("Checking ReRendering");
        return(
           <>
              <h1>{`Pure Component In React ${this.state.count}`}</h1>
              <button onClick={()=>this.setState({count:1})}>Update</button>
           </>

        )
    }
}
export default Pure;