import React, { useState } from "react";
const PostApi=()=>{
    const[title,setTitle]=useState("");
    const[body,setBody]=useState("");
    const saveUser=()=>{
        console.log(title,"\n",body);
        let data={title,body}
        fetch("https://mocki.io/v1/3b95fe53-a2d6-4a0e-bc67-a53794ba10d9",{
            method:"POST",
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
            },
            body:JSON.stringify(data)
        }).then((result)=>{
            //console.warn("result",result);
            result.json().then((respond)=>{
                console.log(respond);
            })
        })
    }
     return <div>
         <h1>POST method API</h1>
         <input type="text" value={title} name="name" onChange={(e)=>setTitle(e.target.value)}/><br/><br/>
         <input type="text" value={body} name="name" onChange={(e)=>setBody(e.target.value)}/><br/><br/>
         <button onClick={saveUser}>Save New User</button>
     </div>

}
export default PostApi;