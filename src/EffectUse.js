import React, {useState} from "react";
import { useEffect } from "react";
const EffectUse=()=>{
    const[item,setItem]=useState(0);
    const[data,setData]=useState(10);
     useEffect(()=>{
         console.log("this is useeffect",item);
        },[data,item])
    return(<>
      <h1>
        effect use  {item} 
      </h1>
      <button onClick={()=>{setItem(item +1)}}>Count</button>
      <button onClick={()=>{setData(data +1)}}>Data</button>
    </>)
}
export default EffectUse;