import React from "react";
const MapFunct=()=>{
    //const students=["Aijaz","Hammad","Sabir","Lateef","Fazal"];
    const details=[
        {name:"Aijaz",email:"aijaz835@gmail.com",pin:190003},
        {name:"Hammad",email:"hammad835@gmail.com",pin:190002},
        {name:"Sabir",email:"sabir835@gmail.com",pin:190001},
        {name:"Lateef",email:"lateef835@gmail.com",pin:190004},
        {name:"Fazal",email:"fazal835@gmail.com",pin:190005}
    ]
    
    return(
        <div>
            <h1>Hey This Tutorial is regarding Map Function And uderstanding Of an Array</h1>
            <table className="p-5 m-5 text-danger">
            <tr>
                         <td>Name</td>
                         <td>Email</td>
                         <td>Pin</td>
                     </tr>
            {
                details.map((friends)=>
                     <tr>
                         <td>{friends.name}</td>
                         <td>{friends.email}</td>
                         <td>{friends.pin}</td>
                     </tr>
                )
            }
            </table>
        </div>
    )


}
export default MapFunct;