import React from "react";
import { useState } from "react";
const MyForm=()=>{
    const[name,setName]=useState("");
    const[choice,setChoice]=useState("");
    const[selekt,setSelekt]=useState(false);
    const getFormData=(e)=>{
         console.log(name,choice,selekt);
         e.preventDefault();
    }
    return(<>
        <h1>Handle Form in React</h1>
        <form onSubmit={getFormData}>
            <input type="text" placeholder="Enter Name" onChange={(event)=>setName(event.target.value)}/><br />
            <select onChange={(event)=>setChoice(event.target.value)}>
                <option>Cricket</option>
                <option>Football</option>
                <option>Tennis</option>
            </select><br /><br />
            <input type="checkbox" onChange={(event)=>setSelekt(event.target.checked)}/>
            <span>Accept Terms and Conditions</span><br /><br />
            <button type="submit">Submit</button>

        </form>
        
    </>)
}
export default MyForm;