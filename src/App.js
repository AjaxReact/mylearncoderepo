import React from "react";
//import UseEffect from "./UseEffect";
import 'bootstrap/dist/css/bootstrap.min.css';
import { useSelector,useDispatch } from "react-redux";
import { decNumber,incNumber } from "./actions/index";


const App=()=>{
   const myState=useSelector((state)=>state.changeTheNumber);
   const dispatch=useDispatch();
   return(<div style={{textAlign:"center"}}>
        {/* <UseEffect/> */}

        <h1>Increment/Decrement Counter</h1>
        <h4>Using React and Redux</h4>
        <a onClick={()=>dispatch(decNumber())}><span>-</span></a>
        <input type="text" value={myState}/>
        <a onClick={()=>dispatch(incNumber())}><span>+</span></a>
   </div>)
}
export default App;