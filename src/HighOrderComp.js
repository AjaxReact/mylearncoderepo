import React, { useState } from 'react';
const HighOrderComp=()=>{
    return(<>
        <h1>HOC</h1>
        <HOCBlue cmp={NewData}/>
        <HOCRed cmp={NewData}/>
        <HOCGreen cmp={NewData}/>
    </>);
}
function HOCBlue(props){
    return <h5 style={{backgroundColor:"blue",width:100}}><props.cmp/></h5>
}
function HOCRed(props){
    return <h5 style={{backgroundColor:"red",width:100}}><props.cmp/></h5>
}
function HOCGreen(props){
    return <h5 style={{backgroundColor:"green",width:100}}><props.cmp/></h5>
}
function NewData(){
    const [value,setValue]=useState(0);
    return <div>
        <h1>{value}</h1>
        <button onClick={()=>setValue(value+1)}>Update</button>
    </div>
}
export default HighOrderComp;
