import React from "react";  
import { useState } from "react";
const ShowHide=()=>{
    const[data,setData]=useState("");
    const[item,setItem]=useState(false);
    const enterValue=(event)=>{
         setData(event.target.value);
        
    }
    return(<>
         {
            item? 
             <h1>
              {data}
             </h1>:
             null
             }
         <input type="text" placeholder="Enter Something" onChange={enterValue} value={data}/>
         <button onClick={()=>{setItem(!item)}}>show/Hide</button>
         
    </>)
}
export default ShowHide;
