import React, { useState } from "react";
const PreviousState=()=>{
    const[counter,setCounter]=useState(1);
    const updateCounter=()=>{
        //setCounter(Math.floor(Math.random()*10));
        setCounter((prev)=>
        {   
            console.log(prev);
            return counter *10; 
            });
    }
   return(<>
        <h1>Previos State {counter}</h1>
        <button onClick={updateCounter}>Click Here</button>
   </>)
}
export default PreviousState;