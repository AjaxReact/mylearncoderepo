import React from 'react';
import { Table } from "react-bootstrap";
const Nested = () => {
    const user = [
        {
            empid: 1, name: "Ajaz", phNO: 7006837497, address: [{ hNo: 10, city: 'Noaida', country: 'India' },
            { hNo: 13, city: 'Gurgaon', country: 'India' }]
        },
        {
            empid: 2, name: "Ajaz", phNO: 7006837497, address: [{ hNo: 10, city: 'Noaida', country: 'India' },
            { hNo: 13, city: 'Gurgaon', country: 'India' }]
        },
        {
            empid: 3, name: "Ajaz", phNO: 7006837497, address: [{ hNo: 10, city: 'Noaida', country: 'India' },
            { hNo: 13, city: 'Gurgaon', country: 'India' }]
        }
    ]
    return (
        <div>
            <h1 className="text-center">Nested List And Map Function</h1>
            <Table striped variant="dark">
                <tbody>
                    <tr>
                        <td>Employee Id</td>
                        <td>Name</td>
                        <td>Phone No</td>
                        <td>Adress</td>
                    </tr>
                    {
                        user.map((value, index) =>
                            <tr key={index}>
                                <td>{value.empid}</td>
                                <td>{value.name}</td>
                                <td>{value.phNO}</td>
                                <td>{value.address.map((data)=>
                                     <Table>
                                         <tbody>
                                             <tr style={{color:'white'}}>
                                                 <td>{data.hNo}</td>
                                                 <td>{data.city}</td>
                                                 <td>{data.country}</td>
                                             </tr>
                                         </tbody>
                                     </Table>
                                )}</td>
                            </tr>
                        )
                    }
                </tbody>
            </Table>

        </div>
    )
}
export default Nested;