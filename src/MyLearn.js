import React from "react";
class MyLearn extends React.Component{
    constructor(){
        super();
        this.state={
            count:0
        }
        console.log("I am Constructor");

    }
    shouldComponentUpdate(){
        console.log("Should component update",this.state.count);
        if(this.state.count>5&&this.state.count<10)
        {
            return true;
        }
        else{
        return false;
        }
    }
    render()
    {
        console.log("I am Render");
        return(<>
            <h1>Should Update Component {this.state.count}</h1>
            <button onClick={()=>{this.setState({count:this.state.count+1})}}>Update</button>
        </>)
    }
}
export default MyLearn;