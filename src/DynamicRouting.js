import React from 'react';
import { Link,Route } from 'react-router-dom';
import Usser from "./Usser";
const DynamicRouting = () => {
    let users = [
        { id: 1, name: 'aijaz', email: 'aijaz@test.com' },
        { id: 2, name: 'Sabir', email: 'sabir@test.com' },
        { id: 3, name: 'fazal', email: 'fazal@test.com' },
        { id: 4, name: 'latif', email: 'latif@test.com' },
        { id: 5, name: 'hammd', email: 'hammd@test.com' }
    ]
    return (<>
        <h1>REACT DYNAMIC ROUTING</h1>
        {
            users.map((val) => <div><Link to={"/user/"+ val.id+"/"+val.name}><h4>{val.name}</h4></Link></div>)

        }
        <Route path="/user/:id/:name"><Usser/></Route>
    </>)
}
export default DynamicRouting;