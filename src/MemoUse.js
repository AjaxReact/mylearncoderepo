import React, { useState,useMemo } from 'react';
const MemoUse=()=>{
    const[data,setData]=useState(0);
    const[item,setItem]=useState(10);
//    function multiMul(){
//        console.warn("Check this one to know why we should use useMemo to enhance performance");
//       return  data*5;
//    }
    const multiMul=useMemo(()=>{
        console.warn("Check this one to know why we should use useMemo to enhance performance");
              return  data*5;
    },[data])
   return(<>
       <h1>UseMemo Concept Data Value: {data}</h1>
       <h1>UseMemo Concept ItemValue: {item}</h1>
       <h1>update value: {multiMul}</h1>
       <button onClick={()=>setData(data+2)}>Update Data</button>
       <button onClick={()=>setItem(item*3)}>Update Item</button>
   </>)
}
export default MemoUse;