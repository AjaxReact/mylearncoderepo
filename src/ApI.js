import React, { useEffect ,useState} from 'react';
import {Table} from 'react-bootstrap';
const ApI=()=>{
    const[data,setData]=useState([]);

    useEffect(()=>{
        fetch("https://mocki.io/v1/0c925950-636f-4c21-bbc1-a1d139a2bd85").then((result)=>{result.json().then((resp)=>{
        setData(resp);
    }
    )});
    },[])
    console.log(data);
    function deleteUser(id){
        fetch(`https://mocki.io/v1/0c925950-636f-4c21-bbc1-a1d139a2bd85/${id}`,{
            method:"DELETE"
        }).then((result)=>{result.json().then((resp)=>{
            console.warn(resp);
    })})
}  
  return <div>
         <h1>Get API Call</h1>
         <Table striped variant="blue">
             <tbody>
                 <tr>
                     <td>userId</td>
                     <td>ID</td>
                     <td>Title</td>
                     <td>Body</td>
                 </tr>
                 {
                     data.map((item,index)=>
                     <tr key={index}>
                     <td>{item.userId}</td>
                     <td>{item.id}</td>
                     <td>{item.title}</td>
                     <td>{item.body}</td>
                     <td><button onClick={()=>deleteUser(item.id)}>Delete</button></td>
                     <td><button > Update</button></td>
                 </tr>
                     )
                 }
             </tbody>
         </Table>
  </div>
}
export default ApI;