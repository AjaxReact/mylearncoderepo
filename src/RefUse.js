import React, { useRef } from 'react';
const RefUse=()=>{
    let refInput=useRef(null);
    const handleInput=()=>{
        //refInput.current.value="50";
        //refInput.current.style.color="blue";
        refInput.current.focus();
    }
    return(<>
       <h1>UseRef Concept</h1>
       <input type="text" ref={refInput}/>
       <button onClick={handleInput}>Manipulate InputBox</button>
    </>)
}
export default RefUse;