import React, { useEffect, useState } from "react";
const UseEffect=()=>{
    const[maal,setMaal]=useState([]);
    const getUsers=async ()=>{
       const response= await fetch("https://jsonplaceholder.typicode.com/users ");
       const data=await response.json();
        console.log(data);
        setMaal(data);
    }
   useEffect(()=>{
       getUsers();
   },[])
   return(<>
       <h2>LIST OF GITHUB USERS</h2>
       {
           maal.map((value,index)=>{
             return  <div key={index}>{value.name}</div>
           })
       }
   </>)
}
export default UseEffect;