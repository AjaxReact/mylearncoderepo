import React,{forwardRef} from 'react';
const ForwardRef=(props,refer)=>{
    return(<>
        <h1>Forward Ref</h1>
        <input type="text" ref={refer} />
    </>)
     
}
export default forwardRef(ForwardRef);